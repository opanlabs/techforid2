$(document).ready(function() {
    
    // slide
    slickInit();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('.multiple-items').slick('unslick');
      slickInit();
    });

    $('.txt-search').click(function(){
      $('.search-form').toggle();
    });
});

function slickInit() {
    $('.multiple-items').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode:true,
        focusOnSelect: true,
        arrows: false
    });
  }